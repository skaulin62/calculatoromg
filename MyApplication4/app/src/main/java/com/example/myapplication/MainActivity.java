package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text1=findViewById(R.id.textView1);
        but1=findViewById(R.id.button1);
        but2=findViewById(R.id.button2);
        but3=findViewById(R.id.button3);
        but4=findViewById(R.id.button4);
        but5=findViewById(R.id.button5);
        but6=findViewById(R.id.button6);
        but7=findViewById(R.id.button7);
        but8=findViewById(R.id.button8);
        but9=findViewById(R.id.button9);
        but0=findViewById(R.id.button0);

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                setTitle("Портретная ориентация");
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                setTitle("Альбомная ориентация");


    }

    TextView text1;
    Button but1, but2,but3, but4,but5, but6,but7, but8,but9, but0;
    float FirstSign;
    String signOperation;
   public void butClearClick(View view) {
        text1.setText("0");
   }

    public void butOneClick(View view) {
        if(text1.getText().equals("0") || text1.getText().equals("Infinity") || text1.getText().equals("NaN") || text1.getText().equals("-Infinity"))
        {
            text1.setText("1");
        }
        else {
            text1.setText(text1.getText().toString()+but1.getText().toString());
        }
    }

    public void butTwoClick(View view) {
        if(text1.getText().equals("0")|| text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity"))
        {
            text1.setText("2");
        }
        else {
            text1.setText(text1.getText().toString()+but2.getText().toString());
        }
    }

    public void butThreeClick(View view) {
        if(text1.getText().equals("0")|| text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity"))
        {
            text1.setText("3");
        }
        else {
            text1.setText(text1.getText().toString()+but3.getText().toString());
        }
    }

    public void butFourClick(View view) {
        if(text1.getText().equals("0")|| text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity"))
        {
            text1.setText("4");
        }
        else {
            text1.setText(text1.getText().toString()+but4.getText().toString());
        }
    }

    public void butFiveClick(View view) {
        if(text1.getText().equals("0")|| text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity"))
        {
            text1.setText("5");
        }
        else {
            text1.setText(text1.getText().toString()+but5.getText().toString());
        }
    }

    public void butSixClick(View view) {
        if(text1.getText().equals("0")|| text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity"))
        {
            text1.setText("6");
        }
        else {
            text1.setText(text1.getText().toString()+but6.getText().toString());
        }
    }

    public void butSevenClick(View view) {
        if(text1.getText().equals("0")|| text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity"))
        {
            text1.setText("7");
        }
        else {
            text1.setText(text1.getText().toString()+but7.getText().toString());
        }
    }
    public void butEightClick(View view) {
        if(text1.getText().equals("0")|| text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity"))
        {
            text1.setText("8");
        }
        else {
            text1.setText(text1.getText().toString()+but8.getText().toString());
        }
    }

    public void butNineClick(View view) {
        if(text1.getText().equals("0")|| text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity"))
        {
            text1.setText("9");
        }
        else {
            text1.setText(text1.getText().toString()+but9.getText().toString());
        }
    }

    public void butZeroClick(View view) {
        if(text1.getText().equals("0")|| text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity"))
        {
            text1.setText("0");
        }
        else {
            text1.setText(text1.getText().toString()+but0.getText().toString());
        }
    }

    public void butPointClick(View view){
       if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
           text1.setText("0");
       }else {
           String checkPoint = text1.getText().toString();

           String point = ".";

           if(checkPoint.lastIndexOf('.')>0) {

           } else {
               text1.setText(text1.getText().toString()+ point);
           }
       }



    }

    public void butPlusClick(View view){
        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            FirstSign = Float.valueOf(text1.getText().toString());
            text1.setText("0");
            signOperation = "+";
        }

    }

    public void butMinusClick(View view) {
        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            FirstSign = Float.valueOf(text1.getText().toString());
            text1.setText("0");
            signOperation = "-";
        }
    }
    public void butMultiplyClick(View view) {
        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            FirstSign = Float.valueOf(text1.getText().toString());
            text1.setText("0");
            signOperation = "*";
        }
    }
    public void butDevisionClick(View view) {
        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            FirstSign = Float.valueOf(text1.getText().toString());
            text1.setText("0");
            signOperation = "/";
        }
    }

    public void butReversePlus(View view) {
        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            Float dolbeb = -1 * (Float.valueOf(text1.getText().toString()));
            text1.setText(dolbeb.toString());
        }
    }
    Float fullDigitProcent;
    public void butProcentClick(View view) {
        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            Float fullDigitProcent = (Float.valueOf(text1.getText().toString())) / 100;
            text1.setText(fullDigitProcent.toString());
            signOperation = "%";
        }
    }
    Float factorialSumm = 1F;
    public void butFactorialClick(View view) {
        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            fullDigitProcent = Float.valueOf(text1.getText().toString());
            for (int i = 1; i <= fullDigitProcent; i++) {
                factorialSumm *= i;
            }
            text1.setText(factorialSumm.toString());
        }
    }
    Float val;
    public void butPI(View view) {
        val = (float)Math.PI;
        text1.setText(val.toString());
    }
    public void butE(View view) {
        val = (float)Math.E;
        text1.setText(val.toString());
    }

    public void butSqrt(View view) {


        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            val = (float) Math.sqrt(Float.valueOf(text1.getText().toString()));
            text1.setText(val.toString());
        }
    }
    public void butSin(View view) {

        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            val = (float) Math.sin(Float.valueOf(text1.getText().toString()));
            text1.setText(val.toString());
        }
    }
    public void butCos(View view) {

        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            val = (float) Math.cos(Float.valueOf(text1.getText().toString()));
            text1.setText(val.toString());
        }
    }
    public void butTg(View view) {

        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            val = (float) Math.tan(Float.valueOf(text1.getText().toString()));
            text1.setText(val.toString());
        }
    }
    public void butCtg(View view) {

        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            val =  1/(float) Math.tan(Float.valueOf(text1.getText().toString()));
            text1.setText(val.toString());
        }
    }

    public void butPow(View view) {
        if(text1.getText().equals("Infinity") || text1.getText().equals("NaN")|| text1.getText().equals("-Infinity")) {
            text1.setText("0");
        }else {
            FirstSign = Float.valueOf(text1.getText().toString());
            text1.setText("0");
            signOperation = "^";
        }
    }

    Float result;
    public void butRavnoClick(View view){
        switch(signOperation){
            case "+":
                result = FirstSign + Float.valueOf(text1.getText().toString());
                text1.setText(result.toString());
                break;
            case "-":
                result = FirstSign - Float.valueOf(text1.getText().toString());
                text1.setText(result.toString());
                break;
            case "*":
                result = FirstSign * Float.valueOf(text1.getText().toString());
                text1.setText(result.toString());
                break;
            case "/":
                result = FirstSign / Float.valueOf(text1.getText().toString());
                text1.setText(result.toString());
                break;
            case "^":
                result = (float) Math.pow(FirstSign,Float.valueOf(text1.getText().toString()));
                text1.setText(result.toString());
                break;

        }
    }
}